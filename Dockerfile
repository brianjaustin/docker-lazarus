FROM debian:buster-slim

# Install FPCLAZUP dependencies (libc6-compat required to run fpcup)
RUN apt-get update && apt-get install -y subversion \
        build-essential libx11-dev libgtk2.0-dev libcairo2-dev \ 
	libpango1.0-dev libxtst-dev libgdk-pixbuf2.0-dev libatk1.0-dev libghc-x11-dev

# Install FPCLAZUP
ADD https://github.com/LongDirtyAnimAlf/Reiniero-fpcup/releases/download/1.6.4f/fpclazup-x86_64-linux /usr/bin/fpcup
RUN chmod a+x /usr/bin/fpcup

# Install FPC and Lazarus
RUN fpcup --fpcURL=fixes3.2 --lazURL=fixes2.0
ENV PATH="/root/development/fpc/bin/x86_64-linux:/root/development/lazarus:${PATH}"
